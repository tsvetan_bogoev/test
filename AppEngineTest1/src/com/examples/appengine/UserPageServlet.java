package com.examples.appengine;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class UserPageServlet extends HttpServlet {
    private static int POKE_DELAY = 20000;
    private static String POKE_MSG = "   :-)";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long userId = (Long) req.getSession().getAttribute("authenticatedUser");
        if (userId != null) {
            String msgToUser = req.getParameter("msgTo");
            if (msgToUser != null && msgToUser.length() > 0) {

                req.setAttribute("msgTo", msgToUser);
                req.getRequestDispatcher("/chat.jsp").forward(req, resp);
            }
            req.getRequestDispatcher("/home.jsp").forward(req, resp);
        } else {
            req.setAttribute("message", "");
            req.getRequestDispatcher("/login.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long userId = (Long) req.getSession().getAttribute("authenticatedUser");
        if (userId != null) {
            if (req.getParameter("listUsers") != null) {
                req.getRequestDispatcher("/listUsers.jsp").forward(req, resp);
            }
            if (req.getParameter("logout") != null) {
                logout(req, resp);
            }
            if (req.getParameter("msgSend") != null) {
                sendMessage(req, resp);
            }
            if (req.getParameter("poke") != null) {
                poke(req, resp);
            }
            if (req.getParameter("search") != null) {
                req.getRequestDispatcher("/listUsers.jsp").forward(req, resp);
            }
        } else {
            if (req.getParameter("sendPoke") != null) {
                sendPoke(req, resp);
            }
            if (!hasValidRequestParameters(req)) {
                req.setAttribute("message", "Please enter valid user name and password");
                req.getRequestDispatcher("/login.jsp").forward(req, resp);
                return;
            }

            if (req.getParameter("signin") != null)
                login(req, resp);
            else
                register(req, resp);
        }
    }

    private boolean hasValidRequestParameters(HttpServletRequest req) {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        return username != null && username.length() > 0 && password != null && password.length() > 0;
    }

    private void register(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        List<User> users = ObjectifyService.ofy().load().type(User.class).filter("name", username).list();
        if (users == null || users.size() == 0) {
            User user = new User(username, password);
            ObjectifyService.ofy().save().entity(user).now();

            req.getSession().setAttribute("authenticatedUser", user.id);

            resp.sendRedirect("/userHome");
        } else {
            req.setAttribute("message", "This username is already used, login ot choose other username");
            req.getRequestDispatcher("/login.jsp").forward(req, resp);
        }
    }

    private void login(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        Key<User> userKey = Key.create(User.class, username);
        List<User> users = ObjectifyService.ofy().load().type(User.class).filter("name", username).list();
        if (users == null || users.size() != 1) {
            req.setAttribute("message", "No such username");
            req.getRequestDispatcher("/login.jsp").forward(req, resp);
        } else {
            User user = users.get(0);
            if (user.password.compareTo(password) != 0) {
                req.setAttribute("message", "Wrong password");
                req.getRequestDispatcher("/login.jsp").forward(req, resp);
            } else {
                req.getSession().setAttribute("authenticatedUser", user.id);
                resp.sendRedirect("/userHome");
            }
        }
    }

    private void logout(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        req.getSession();
        req.getRequestDispatcher("/login.jsp").forward(req, resp);
    }

    private void poke(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String msgToUser = req.getParameter("msgTo");

        Queue queue = QueueFactory.getDefaultQueue();
        queue.add(TaskOptions.Builder.withUrl("/userHome")
                .param("sendPoke", "")
                .param("msgTo", msgToUser)
                .param("msgFrom", req.getSession().getAttribute("authenticatedUser").toString())
                .countdownMillis(POKE_DELAY));

        req.setAttribute("msgTo", msgToUser);
        req.getRequestDispatcher("/chat.jsp").forward(req, resp);


    }

    private void sendMessage(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String msg = req.getParameter("msg");
        if (msg != null && msg.length() > 0) {

            Long userId = (Long) req.getSession().getAttribute("authenticatedUser");
            String msgToUser = req.getParameter("msgTo");

            Message message = new Message(userId, Long.parseLong(msgToUser), msg);
            ObjectifyService.ofy().save().entity(message).now();

            req.setAttribute("msgTo", msgToUser);
            req.getRequestDispatcher("/chat.jsp").forward(req, resp);
        }
    }

    private void sendPoke(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Message message = new Message(
                Long.parseLong(req.getParameter("msgFrom")),
                Long.parseLong(req.getParameter("msgTo")),
                POKE_MSG);
        ObjectifyService.ofy().save().entity(message).now();
    }
}
