package com.examples.appengine;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.util.Date;

@Entity
public class Message {
    @Id
    Long id;
    @Index
    Long from;
    @Index
    Long to;

    String msg;
    Date timestamp;

    @Index
    boolean read;

    private Message() {
    }

    Message(Long from, Long to, String msg) {
        this.from = from;
        this.to = to;
        this.msg = msg;
        this.timestamp = new Date();
    }

    public Date getTimestamp() {
        return timestamp;
    }


    public Long getFrom() {
        return from;
    }

    public Long getTo() {
        return to;
    }

    public String getMsg() {
        return msg;
    }

}
