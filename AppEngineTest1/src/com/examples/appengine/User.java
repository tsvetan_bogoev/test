package com.examples.appengine;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class User {
    @Id
    Long id;
    @Index
    String name;
    String password;

    private User()
    {

    }

    public User(String name, String password)
    {
        this.name = name;
        this.password = password;
    }


    public String getName(){
        return  name;
    }
    public Long getId(){
        return  id;
    }
}

