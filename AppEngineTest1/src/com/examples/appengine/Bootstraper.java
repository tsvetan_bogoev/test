package com.examples.appengine;

import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class Bootstraper implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        ObjectifyService.register(User.class);
        ObjectifyService.register(Message.class);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}