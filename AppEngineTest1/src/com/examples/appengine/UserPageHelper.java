package com.examples.appengine;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class UserPageHelper {
    protected HttpServletRequest req;

    private  UserPageHelper(HttpServletRequest request) {
        this.req = request;
    }

    public static UserPageHelper create(HttpServletRequest request) {
        return new UserPageHelper(request);
    }

    public String getUsername(Long userId) {
        User user = ObjectifyService.ofy().load().type(User.class).id(userId).now();
        if(user != null)
            return user.getName();
        else
            return null;
    }

    public String getUsername() {
        return getUsername(getUserId());
    }

    public Long getUserId() {
        return (Long)req.getSession().getAttribute("authenticatedUser");
    }

    public List<Message> listUnreadMessages() {
        List<Message> unreadMessages = ObjectifyService.ofy().load().type(Message.class)
                .filter("to", getUserId())
                .filter("read", false).list();
        unreadMessages.sort(Comparator.comparing(Message::getTimestamp));

        //Mark them as read
        List<Message> tmpMessages = new ArrayList<>(unreadMessages);
        for (Message msg : tmpMessages) {
            msg.read = true;
        }
        ObjectifyService.ofy().save().entities(tmpMessages).now();

        return unreadMessages;
    }

    public List<Message> listMessages() {
        List<Message> allMessages = new ArrayList<>();

        Long getUserId = getUserId();
        String msgToUser = req.getParameter("msgTo");

        if (msgToUser != null && msgToUser.length() > 0) {
            Long msgToId = Long.parseLong(req.getParameter("msgTo"));
            List<Message> sentMessages = ObjectifyService.ofy().load().type(Message.class)
                    .filter("from", getUserId)
                    .filter("to", msgToId).list();
            allMessages.addAll(sentMessages);

            List<Message> receivedMessages = ObjectifyService.ofy().load().type(Message.class)
                    .filter("from", msgToId)
                    .filter("to", getUserId).list();
            allMessages.addAll(receivedMessages);
        }

        allMessages.sort(Comparator.comparing(Message::getTimestamp));
        return allMessages;
    }

    private String nextCursorStr = "";
    public String getNextCursorStr(){
        return nextCursorStr;
    }

    public List<User> listUsers() {
        String searchStr = req.getParameter("searchStr");
        List<User> users;
        if (searchStr != null && searchStr.length() > 0)
        {

            //Search users, does not use cursor for now
            users = ObjectifyService.ofy().load().type(User.class)
                    .filter("name >=", searchStr)
                    .filter("name <", searchStr + "\uFFFD")
                    .list();
        }
        else {
            users = new ArrayList<>();
            Query<User> query = ObjectifyService.ofy().load().type(User.class).order("name").limit(5);

            String cursorStr = req.getParameter("cursor");
            if (cursorStr != null)
                query = query.startAt(Cursor.fromWebSafeString(cursorStr));

            QueryResultIterator<User> iterator = query.iterator();
            while (iterator.hasNext()) {
                users.add(iterator.next());
            }

            if (users.size() == 5) {
                Cursor cursor = iterator.getCursor();
                nextCursorStr = new String(cursor.toWebSafeString());
            }else
                nextCursorStr = "";
        }

        return users;
    }
}
