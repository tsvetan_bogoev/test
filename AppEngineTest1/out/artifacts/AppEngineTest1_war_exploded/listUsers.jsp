<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="utils" prefix="utils"%>
<h3>
    <c:set var="userPageHelper" value="${utils:createHelper(pageContext.request)}"/>
    <c:set var="username" value="${userPageHelper.getUsername()}"/>

    Hello <c:out value="${username}"/>

    <form method="POST" action="${destination}">
        <button type="submit" name="listUsers" class="btn btn-success">List users</button>
        <button type="submit" name="logout" class="btn btn-success">Logout</button>
    </form>


    <form method="POST">
        <div class="form-group">
            <input type="text" name="searchStr" id="search" value="${fn:escapeXml(searchStr)}" class="form-control" />
            <button type="submit" name="search" class="btn btn-success">Search</button>
        </div>
    </form>

<%-- *********************** Users list *********************** --%>
    <c:set var="users" value="${userPageHelper.listUsers()}"/>
    <c:choose>
        <c:when test="${not empty users}">
            <c:forEach items="${users}" var="usr">
                <c:choose>
                    <c:when test="${usr.getName() != username}">
                        <div class="media">
                            <a href="/userHome?msgTo=${usr.getId()}">
                                <h4>${usr.getName()}</h4>
                            </a>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <h4>${usr.getName()}</h4>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </c:when>
    </c:choose>


    <c:choose>
        <c:when test="${userPageHelper.getNextCursorStr().length()>0}">
            <form method="POST">
                <div class="form-group">
                    <input type="hidden" name="cursor" value="${userPageHelper.getNextCursorStr()}">
                    <button type="submit" name="listUsers" class="btn btn-success">Next</button>
                </div>
            </form>
        </c:when>
    </c:choose>
</h3>
