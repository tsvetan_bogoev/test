<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="utils" prefix="utils"%>
<h3>
    <c:set var="userPageHelper" value="${utils:createHelper(pageContext.request)}"/>
    <c:set var="username" value="${userPageHelper.getUsername()}"/>

    Hello <c:out value="${username}"/>

    <form method="POST">
        <button type="submit" name="listUsers" class="btn btn-success">List users</button>
        <button type="submit" name="logout" class="btn btn-success">Logout</button>
    </form>


    <%-- *********************** Chat *********************** --%>
    <c:set var="messages" value="${userPageHelper.listMessages()}"/>

    <c:choose>
        <c:when test="${not empty messages}">
            <h4>Messages:</h4>
            <c:forEach items="${messages}" var="message">
                    <%--Do we need to optimize DB read for getUsername?????????????????????? --%>
                    <p>${userPageHelper.getUsername(message.getFrom())}:<i>${message.getMsg()}</i></p>
            </c:forEach>
        </c:when>
    </c:choose>
    <c:choose>
        <c:when test="${param['msgTo'].length()>0}">
             <form method="POST">
                <h4>Send message to ${userPageHelper.getUsername(param['msgTo'])}</h4>
                <div class="form-group">
                    <label for="msg">Text:</label>
                    <textarea rows="4" cols="50" name="msg" id="msg"  name="comment" value="${fn:escapeXml(msg)}"></textarea>
                </div>
                <button type="submit" name="msgSend" class="btn btn-success">Send</button>
            </form>
            <form method="POST">
                <button type="submit" name="poke" class="btn btn-success">Poke</button>
            </form>
        </c:when>
    </c:choose>
</h3>
