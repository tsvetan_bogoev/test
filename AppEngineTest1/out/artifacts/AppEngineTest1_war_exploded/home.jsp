<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="utils" prefix="utils"%>
<h3>
    <c:set var="userPageHelper" value="${utils:createHelper(pageContext.request)}"/>
    <c:set var="username" value="${userPageHelper.getUsername()}"/>

    Hello <c:out value="${username}"/>

    <form method="POST" action="${destination}">
        <button type="submit" name="listUsers" class="btn btn-success">List users</button>
        <button type="submit" name="logout" class="btn btn-success">Logout</button>
    </form>


    <%-- *********************** Unread messages*********************** --%>
    <c:set var="unreadMessages" value="${userPageHelper.listUnreadMessages()}"/>

    <c:choose>
        <c:when test="${not empty unreadMessages}">
            <h4>NEW Messages:</h4>
            <c:forEach items="${unreadMessages}" var="message">
                <p><b>${userPageHelper.getUsername(message.getFrom())}:<i>${message.getMsg()}</i></b></p>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <h4>No new Messages!</h4>
        </c:otherwise>
    </c:choose>

</h3>
