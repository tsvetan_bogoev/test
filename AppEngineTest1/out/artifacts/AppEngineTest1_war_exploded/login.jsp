<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div class="container">
    <h3>
        <c:out value="${message}" />
    </h3>

  <form method="POST" action="signup">

    <div class="form-group">
      <label for="username">Username</label>
      <input type="text" name="username" id="username" value="${fn:escapeXml(username)}" class="form-control" />
    </div>

    <div class="form-group">
      <label for="password">Password</label>
      <input type="text" name="password" id="password" value="${fn:escapeXml(password)}" class="form-control" />
    </div>

    <button type="submit" name="signin" class="btn btn-success">Login</button>
    <button type="submit" name="signup" class="btn btn-success">Register</button>
  </form>
</div>
